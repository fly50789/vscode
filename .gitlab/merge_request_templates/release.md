> (Title name format: [Project-name] version [version-number] release) <br>
> (if there has major feature change, please change major version number, if if only has patchs, add small version number ) <br>
> i.e. [SVP] Version 3.0 release <br>
> i.e. [SVP] Version 3.1 release <br>
---


# Objective 
###### (New feature description in detail in top-down list, i.e. Test Group editor)
- 


---
> **(Please follow the procedure below to set this items in the issue)** <br>
> Assignee: (use **/assign @name** to set assignee of this merge request) <br>
/assign 
> Reviewer: (use **/assign_reviewer @name** to set reviewer of this merge request) <br>
/assign_reviewer
> Milestone: (use **/milestone %milestone** to set dedicated milestone) <br>
/milestone 
> Label： (use **/label ~label** to set label on issue, need label: issue-type & status) <br>
/label 
