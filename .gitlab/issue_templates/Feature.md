> (Title name format: [Project-name](title-name)-(phase-number:optional)) <br>
> (if issue contains multiple phase, please create 1 major issue and  N sub-issues) <br>
> i.e. **[SVP]Test script editor page** <br>
> i.e. **[SVP]Test script editor page - phase 1** <br>
> i.e. **[SVP]Test script editor page - phase 2** <br>
---


# Objective 
###### (Specfic task description in detail in top-down list, i.e. Create test script editor contents in svp-portal Nav bar)
- 

---
> **(Please follow the procedure below to set this items in the issue)** <br>
> Assignee: (use **/assign @name** to set assignee of this feature) <br>
/assign 
> Due Date: (use **/due date** to set task's deadline i.e. in 2 days | this Friday | December 1st) <br>
/due  
> Milestone: (use **/milestone %milestone** to set dedicated milestone) <br>
/milestone 
> Label： (use **/label ~label** to set label on issue, need label: assignee-name & issue-type) <br>
/label 
> Estimated time: (use **/estimate time** to set estimated developed time i.e. 1w 2d 5h) <br>
/estimate
> Spend time: (during the development, please use **/spend time** to take spend time note) <br>