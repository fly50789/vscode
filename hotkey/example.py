try:
    import colored_traceback.always
except:
    pass

'''
https://code.visualstudio.com/shortcuts/keyboard-shortcuts-windows.pdf

ctrl+k ctrl+s

'''

def f2():
    if 1:
        print('something')
        if 1:
            pass
class Calculator():
    
    def __init__(self) -> None:
        self.tmp = 0
        
    def add(self,a,b):       
        return a+b
    def sub(self,a,b):        
        return a+b
    
# Basic editing    
def ctrl_c()    :
    '複製文字'
def ctrl_x()    :
    '剪下文字'
def alt_up_down()    :
    '根據指標的位置移動行'
    1
    2
    3
def ctrl_shift_k()    :
    '刪掉行'
def ctrl_enter()    :
    '插入行之後 但是不能用其實按enter就可以了'
def ctrl_shift_enter()    :
    '插入行之前'
def ctrl_shift_backslash():#\
        '''jump bracket
        會跳到結尾 如果已經在結尾
        跳回頭 非常強大的功能
        '''
        {
            'A':[[12,3,],
                
                ],
















        }
    
def ctrl_square_brackets():
    '移動indent等同 tab/shift+tab'
def home_end():
    '行的頭尾'    
def ctrl_home_end():
    '檔案的頭尾'
    print()
def ctrl_up_down():
    '移動行位置但不移動指標'    
def alt_pgup_pgdown():
    '移動區塊位置但不移動指標' 
def ctrl_shift_square_brackets():# [ ] 
        '摺疊展開區塊' 
def ctrl_k_ctrl_square_brackets():
    if 1:
        '摺疊展開子區塊' 
        if 1:
            pass
def ctrl_k_ctrl_zero():'摺疊所有區塊' 
def ctrl_k_ctrl_j(): '展開所有區塊' 
def ctrl_k_ctrl_c(): #ctrl+/
    '增加 comment' 
 
def ctrl_k_ctrl_u(): 
    '刪除 comment' 
def ctrl_back_slash():#/
    '上面的不方便用這個就好切換commet'
def shift_alt_a():#/
    
    ''' 
    選取區塊後可以快速commet非常有用
    
    Keyword arguments:
    argument -- description
    Return: return_description
    '''
    
    # a
    123


def alt_z():
    ''' word warp                                                      '''

# Navigation
def ctrl_t()        :
    ''' 可以快速在function之間切換 ctrl_t=>f
        基於breadcrumbs得名稱
        左邊的大綱也可以跳    
    '''
def ctrl_g() :''' 跳到特定行 '''
def ctrl_p() :''' 跳到特定檔案跟ctrl_shift_p很像 '''
def ctrl_shift_o():''' 也是跳function用的 '''
def shift_f8():''' 跳到錯誤位置 加shift是跳到前一個 '''

def ctrl_shift_tab():'''上面位置跳到不同檔案 '''

def alt_left_right():
    ''' 跳到指標原來的位置
    非常重要的功能
    
    '''
    a = 5
    b= 6

def Ctrl_M():
    '''  Toggle Tab moves focus 我覺得沒啥用還是用滑鼠八 
    如果突然不能用TAB移動ident可以看下面status bar
    看是不是按到 table focus
    
    '''
    
    
# Search and replace
def Ctrl_F(): ''' Find '''
def Ctrl_H(): ''' Replace '''
def Shift_F3(): ''' Find next/previous '''
def alt_Shift_F3(): ''' Find git difference '''
def Alt_Enter(): ''' 選擇所有符合的 '''
def Ctrl_D(): ''' 複製選取範圍的字貼上尋找 '''
def Ctrl_K_Ctrl_D(): 
    ''' 選擇範圍內的字貼到搜尋上面 
    相當於ctrl+c ctrl+f ctrl+v
    '''
def Alt_CRW(): 
    '''
    切換搜尋模式
     case-sensitive / regex / whole word
    '''
    
#Multi-cursor and selection
#目前沒有要講
def f2(a:int,b:str)-> None:
    '''
    docstring
    '''
    pass
print()

#Rich languages editing
def Ctrl_I():
    '''顯示建議'''.split()
def Ctrl_Shift_I():    
    '''顯示＿doc__內容
    ctrl+k ctrl+s
pylance.triggerparameterhints
    
    '''.split()
    
def Shift_Alt_F():'''  格式化文件'''    
def Ctrl_K_Ctrl_F():'''  格式化選擇的部分'''    
def F12():''' 打開該檔案看定義 '''
def Alt_F12():''' 打開一個暫時的視窗看定義 '''
def Ctrl_K_F12():''' 打開該檔案到切割的視窗看定義 '''
def Ctrl_DOT():''' Quick Fix 用途不明 '''
def Shift_F12():''' 顯示所有有用到的地方 '''
def F2():''' 改名字可以先看會同步改到哪 '''
def Ctrl_K_Ctrl_X():      ''' 刪掉額外的空白 '''             
def Ctrl_K_M():'''   可以直接改變要用那種語言跑   '''



if __name__ == '__main__':
    alt_left_right()
    print('wtf')





