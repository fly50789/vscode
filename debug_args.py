
import argparse
import sys

parser = argparse.ArgumentParser(description='Description of your program')
parser.add_argument('-s', '--server_api', help='server_api URL')
parser.add_argument('-l', '--auto_loc', help='auto tool location')
parser.add_argument('-p', '--sys_print', help='print system log')
parser.add_argument('-d', '--debug_level', help='auto tool location')
parser.add_argument('-t', '--trace_all', help='error trace all')
parser.add_argument('-e', '--env', help='enviroment', type=str)

if __name__ == "__main__":
    print(sys.argv)
    print(vars(parser.parse_args()))
